package Visual;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;

import logica.Contador;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ventana {

	JFrame frame;
	private JButton btnIncrementar;
	private JLabel lblcantidad_presionada;
	private JButton btnGuardar;
	
	private Contador contador;
	private JLabel lblNewLabel;

	
	/**
	 * Create the application.
	 */
	public Ventana(Contador cont) {
		this.contador = cont;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		 btnIncrementar = new JButton("PRESIONAME");
		btnIncrementar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnIncrementar.setBounds(95, 52, 264, 119);
		frame.getContentPane().add(btnIncrementar);
		
		 lblcantidad_presionada = new JLabel("0");
		lblcantidad_presionada.setFont(new Font("Tahoma", Font.PLAIN, 31));
		lblcantidad_presionada.setBounds(214, 182, 52, 56);
		frame.getContentPane().add(lblcantidad_presionada);
		
		btnGuardar = new JButton("Guardar y limpiar");

		btnGuardar.setBounds(10, 11, 143, 23);
		frame.getContentPane().add(btnGuardar);
		
		lblNewLabel = new JLabel("Presiona el bot�n todas las veces que puedas");
		lblNewLabel.setBounds(84, 236, 305, 14);
		frame.getContentPane().add(lblNewLabel);
		
		 btnIncrementar.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent arg0) {
			 		
			 		contador.incrementar();
			 		lblcantidad_presionada.setText(contador.getCuenta()+"");
			 	}
			 });
		 
		 
		btnGuardar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					contador.guardarConteo();
					lblcantidad_presionada.setText("0");
				}
			});
		
	}
}






